#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <dirent.h>


using namespace std;

typedef struct
{
    vector< vector<long double> > table;
    vector< vector<long double> > weights_table;
    long correct;
    vector<long double> min;
    vector<long double> max;
} DATA;

DATA data;

vector< vector<long double> > exteract_data(string file_name)
{
    ifstream file (file_name);
    string line;
    getline(file,line);
    string val;
    vector< vector<long double> > table;
    while(getline(file,line)){
        
        vector<long double> v;
        stringstream s(line);
        
        while(getline(s,val, ','))
            v.push_back(stof(val));
        
        table.push_back(v);
    }
    file.close();
    return table;
}

void find_min()
{
    for(int row = 0; row < data.table.size(); row ++)
    {
        for(int col = 0; col < data.table[row].size(); col++)
        {
            if(data.min.size() <= col)
            {
                data.min.push_back(data.table[row][col]);
            }
            else
            {
                if(data.min[col] > data.table[row][col]) data.min[col] = data.table[row][col];
            }
        }
    }
}

void find_max()
{
    
    for(int row = 0; row < data.table.size(); row ++)
    {
        for(int col = 0; col < data.table[row].size(); col++)
        {
            if(data.max.size() <= col)
            {
                data.max.push_back(data.table[row][col]);
            }
            else
            {
                if(data.max[col] < data.table[row][col]) data.max[col] = data.table[row][col];
            }
        }
    }
}

int predict(int i)
{
    long double max_score = 0;
    int price_range;
    for(int k = 0; k< data.weights_table.size(); k++)
    {
        long double score = 0;
    
        for (int j = 0; j < data.table[i].size() - 1; j++)
            score += (data.table[i][j] * data.weights_table[k][j]);
                    
        score += data.weights_table[k][data.table[i].size()-1];
                    
        if(score > max_score)
        {
            max_score = score;
            price_range = k;
        }
    }
    return price_range;
}
int main(int argc, char *argv[])
{
    char* folder = argv[1];
    string directory(folder);
    string temp = "./" + directory;
    char* datasets = (char*) temp.c_str();
    DIR* dirp = opendir(datasets);
    
    string train_file_name = string(folder) +"train.csv";
    string weights_file_name = string(folder) +"weights.csv";
    
    
    data.table = exteract_data(train_file_name);
    data.weights_table = exteract_data(weights_file_name);
    
    find_min();
    find_max();
    
    long correct = 0;
    for(long i = 0; i<data.table.size(); i++)
    {
        for(long j = 0; j < data.table[i].size() - 1; j++)
        {
            double temp = data.table[i][j] - data.min[j];
            data.table[i][j] = temp / (data.max[j] - data.min[j]);
        }
        int price_range = predict(i);
        if(price_range == data.table[i][data.table[i].size() - 1]) correct++;
    }
    long double accuracy = ((long double)correct) / (data.table.size());
    
    printf("Accuracy = %0.2Lf \n",accuracy * 100);
    
    
    
    
    return 0;
}

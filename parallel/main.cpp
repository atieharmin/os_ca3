#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <dirent.h>
#include <pthread.h>


using namespace std;


#define NUMBER_OF_THREADS 4

typedef struct
{
    vector< vector<long double> > table0;
    vector< vector<long double> > table1;
    vector< vector<long double> > table2;
    vector< vector<long double> > table3;
    vector< vector<long double> > weights_table;
    long table_num;
    long correct;
    vector<long double> min;
    vector<long double> max;
    long size;
    char* folder;
} DATA;

DATA data;

pthread_t threads[NUMBER_OF_THREADS];

pthread_mutex_t mutex_correct;

void find_minmax(int table_num)
{
    vector< vector<long double> > table;
    if(table_num == 0)
        table = data.table0;
    if(table_num == 1)
        table = data.table1;
    if(table_num == 2)
        table = data.table2;
    if(table_num == 3)
        table = data.table3;
    for(int row = 0; row < table.size(); row ++)
    {
        for(int col = 0; col < table[row].size(); col++)
        {
            if(data.min.size() <= col)
            {
                data.min.push_back(table[row][col]);
            }
            else
            {
                if(data.min[col] > table[row][col])
                    data.min[col] = table[row][col];
            }
            
            if(data.max.size() <= col)
            {
                data.max.push_back(table[row][col]);
            }
            else
            {
                if(data.max[col] < table[row][col])
                    data.max[col] = table[row][col];
            }
        }
    }
    pthread_exit(NULL);
}

void* predict_price_class(void* arg)
{
    long correct = 0;
    long table_num = (long)arg;
    vector< vector<long double> > table;
    if(table_num == 0)
        table = data.table0;
    if(table_num == 1)
        table = data.table1;
    if(table_num == 2)
        table = data.table2;
    if(table_num == 3)
        table = data.table3;
    
    for(long i = 0; i<table.size(); i++)
    {
        long double max_score = 0;
        int price_range = 0;
        for(long j = 0; j < table[i].size() - 1; j++)
        {
            double temp = table[i][j] - data.min[j];
            table[i][j] = temp / (data.max[j] - data.min[j]);
        }
        for(int k = 0; k < data.weights_table.size(); k++)
        {
            long double score = 0;
            
            for (int l = 0; l < table[i].size() - 1; l++)
                score += (table[i][l] * data.weights_table[k][l]);
        
            score += data.weights_table[k][table[i].size()-1];
                        
            if(score > max_score)
            {
                max_score = score;
                price_range = k;
            }
        }
        if(price_range == table[i][table[i].size() - 1]) correct++;
    }
    pthread_mutex_lock (&mutex_correct);
    data.correct += correct;
    pthread_mutex_unlock (&mutex_correct);
    pthread_exit(NULL);
}

void* extract_train_data(void* arg)
{
    char* folder = data.folder;
    long table_num = (long)arg;
    string train_file_name = string(folder) +"train_" + to_string(table_num) + ".csv";
    ifstream train_file (train_file_name);
    string train_line;
    getline(train_file,train_line);
    string val;
    while(getline(train_file,train_line)){
        
        vector<long double> v;
        stringstream train_s(train_line);
        
        while(getline(train_s,val, ','))
            v.push_back(stof(val));
        
        if(table_num == 0)
            data.table0.push_back(v);
        if(table_num == 1)
            data.table1.push_back(v);
        if(table_num == 2)
            data.table2.push_back(v);
        if(table_num == 3)
            data.table3.push_back(v);
    }
    train_file.close();
    find_minmax(table_num);
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    void *status;
    char* folder = argv[1];
    string directory(folder);
    string temp = "./" + directory;
    char* datasets = (char*) temp.c_str();
    DIR* dirp = opendir(datasets);
    
    string weights_file_name = string(folder) +"weights.csv";
    
    ifstream weight_file (weights_file_name);

    string weight_line;
        
    getline(weight_file,weight_line);

    string val;
        
    while(getline(weight_file,weight_line)){
        
        vector<long double> v;
        stringstream weight_s(weight_line);
        
        while(getline(weight_s,val, ','))
            v.push_back(stof(val));
        
        data.weights_table.push_back(v);
    }
    weight_file.close();
    
    data.folder = folder;
    
    for(long i = 0; i < NUMBER_OF_THREADS; i++)
        pthread_create(&threads[i], NULL, extract_train_data, (void*)i);
    
    for(long i = 0; i < NUMBER_OF_THREADS; i++)
        pthread_join(threads[i], &status);
    
    pthread_mutex_init(&mutex_correct, NULL);
    for(long i = 0; i < NUMBER_OF_THREADS; i++)
        pthread_create(&threads[i], NULL, predict_price_class, (void*)i);
    
    for(long i = 0; i < NUMBER_OF_THREADS; i++)
        pthread_join(threads[i], &status);
    
    data.size = NUMBER_OF_THREADS * data.table0.size();

    long double accuracy = ((long double)data.correct) / (data.size);
    
    pthread_mutex_destroy(&mutex_correct);
    printf("Accuracy = %0.2Lf \n",accuracy * 100);
    pthread_exit(NULL);
}

